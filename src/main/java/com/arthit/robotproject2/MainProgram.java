/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.robotproject2;

import java.util.Scanner;

/**
 *
 * @author Arthit
 */
public class MainProgram {
    public static void main(String[] args) {
        TableMap map = new TableMap (10,10);
        Scanner kb = new Scanner (System.in);
        Robot robot = new Robot (2,2,'x',map);
        Bomb bomb = new Bomb (5,5);
        map.setRobot(robot);
        map.setBomb(bomb);
        
        
        while(true){
            map.Showmap();
            char Direction = Input(kb);
            robot.walk(Direction);
            if(Direction=='q'){
                break;
            }
            
        }
    }

    private static char Input(Scanner kb) {
        char Direction = kb.next().charAt(0);
        return Direction;
    }
}
