/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.robotproject2;

/**
 *
 * @author Arthit
 */
class TableMap {
    private int width;
    private int height;
    private Robot robot;
    private Bomb bomb;

    public TableMap(int width, int height) {
        this.width = width;
        this.height = height;
    }
    
    public void Showmap(){
        ShowTitle();
        for(int y =0 ; y< height ;y++){
            for(int x = 0; x<width; x++){
                if(robot.isOn(x, y)){
                    ShowRobot();
                }else if (bomb.isOn(x, y)){
                    ShowBomb();
                }else {
                    ShowLine();
                }    
            }Enter();
            
        }
    }
    private void ShowTitle() {
        System.out.println("Map");
    }

    private void ShowBomb() {
        System.out.print(bomb.getSymbol());
    }

    private void Enter() {
        System.out.println("");
    }

    private void ShowLine() {
        System.out.print("-");
    }

    private void ShowRobot() {
        System.out.print(robot.getSymbol());
    }
    
    public boolean inmap(int x , int y){
        return x<width && x>=0 && y>=0 && y<height;
    }
    
     public boolean isBomb(int x , int y){
        return bomb.isOn(x, y);
    }
     
     public void setBomb (Bomb bomb){
         this.bomb = bomb;
     }

    public void setRobot(Robot robot) {
       this.robot = robot;
    }
    
}
